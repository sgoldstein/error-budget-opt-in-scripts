require 'yaml'

GROUP_NAME = ARGV[0]
if GROUP_NAME.empty?
  warn "usage: #{$0} stage [stage...]"
end

def categories_by_group
  @categories_by_group ||= (
    text = File.read(File.join(File.dirname(__FILE__), *%w|.. www-gitlab-com data stages.yml|))
    data = YAML.load(text)
    categories_by_group = data['stages'].inject({}) do |memo, (stage_name, stage_data)|
      stage_data['groups'].each do |(group_name, group_data)|
         memo["#{stage_name}:#{group_name}"] = group_data['categories']
      end
      memo
    end
  )
end

def update_category_references(*categories)
  system('vi', *`ack -l 'feature_category.*(#{categories.join('|')})' '../gitlab' | grep -v '/workers/'`.split)
end

def update_group_references(group_name)
  update_category_references(categories_by_group[group_name])
end

update_group_references(GROUP_NAME)


